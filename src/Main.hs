module Main where


import qualified Data.Map as Map
import Data.Map
  ( Map
  )
import Data.Maybe
import qualified Data.Set as Set
import Data.Set
  ( Set
  )
import Data.Semigroup
import Control.Monad
import Control.Exception
  ( assert
  )
-- import System.IO.Unsafe

import TM
-- import TransitionAccessible
import TM.Run
  ( runTM
  , Status (..)
  , ListTape (..)
  , Runtime (..)
  , initialRuntime
  , followTransition
  )
import TM.Tape
import Ordinal


data WrittenStatus
  = WrittenStatus
    { tWritten ::
      Bool
    , fWritten ::
      Bool
    }
  deriving
    ( Eq
    )


data EventRecord
  = EventRecord
    { directionUsed ::
      Bool
    , pretraversalWrittenStatus ::
      WrittenStatus
    , writtenStatus ::
      WrittenStatus
    , minUnusedIndex ::
      StateIndex
    , stronglyAccessibleTransitions ::
      Set TransitionIndex
    }


instance Semigroup WrittenStatus where
  WrittenStatus tw1 fw1 <> WrittenStatus tw2 fw2 =
    WrittenStatus
      { tWritten =
        getAny $ Any tw1 <> Any tw2
      , fWritten =
        getAny $ Any fw1 <> Any fw2
      }


instance Monoid WrittenStatus where
  mempty =
    WrittenStatus
      { tWritten = False
      , fWritten = False
      }


wroteT :: WrittenStatus -> WrittenStatus
wroteT prev =
  prev { tWritten = True }


wroteF :: WrittenStatus -> WrittenStatus
wroteF prev =
  prev { fWritten = True }


type Queue a =
  [ a ]


pop ::
  ( )
    => Queue a -> Maybe (a, Queue a)
pop (a : b) =
  Just (a, b)
pop [] =
  Nothing


insert ::
  ( )
    => [ a ] -> Queue a -> Queue a
insert xs q =
  q ++ xs


data EventRecord' =
  EventRecord'
    { traveledDirection :: Bool
    , nextUnusedNode :: StateIndex
    }


instance Semigroup EventRecord' where
  er1 <> er2 =
    EventRecord'
      { traveledDirection =
        traveledDirection er1 || traveledDirection er2
      , nextUnusedNode =
        nextUnusedNode er1 `max` nextUnusedNode er2
      }


data IncompleteTM =
  IncompleteTM
    { maybeLimit ::
      Maybe StateIndex
    , partialTransitions ::
      Map TransitionIndex Transition
    , size' ::
      StateIndex
    }


instance Show IncompleteTM where
  show incompleteTM =
    unlines $
      concat
        [ [ show (size' incompleteTM) ++ " node(s)" ]
        , [ "Start -> q0" ]
        , maybeToList $ fmap (("Limit -> q" ++) . show) $ maybeLimit incompleteTM
        , Map.foldMapWithKey (fmap pure . showFullTransition) $ partialTransitions incompleteTM
        ]


generate' ::
  (
  )
    => StateIndex -> Integer -> [ IncompleteTM ]
generate' numNodes =
  runAndGenerate
    initialRuntime
    IncompleteTM
      { maybeLimit =
        Nothing
      , partialTransitions =
        Map.empty
      , size' =
        numNodes
      }
    EventRecord'
      { traveledDirection =
        False
      , nextUnusedNode =
        1
      }
  where
    runAndGenerate :: Runtime ListTape Bool -> IncompleteTM -> EventRecord' -> Integer -> [ IncompleteTM ]
    runAndGenerate runtime incompleteTM eventRecord remainingSteps =
      let
        nextTransitionIndex :: TransitionIndex
        nextTransitionIndex =
          TransitionIndex
          { stateTrigger =
            stateIndex runtime
          , valueTrigger =
            readHead $ tape runtime
          }
      in
        if
          remainingSteps <= 0
        then
          pure incompleteTM
        else if
          Map.size (Map.delete nextTransitionIndex $ partialTransitions incompleteTM) >= 2*numNodes - 1
        then
          do
            guard $ time runtime >= Ordinal 1 0
            pure incompleteTM
        else
          do
            let
              possibleTransitions :: [ Transition ]
              possibleTransitions =
                do
                  transitionDir <- [ Up, Down, None ]
                  guard $ transitionDir /= Down || traveledDirection eventRecord
                  transitionWrite <- [ True, False ]
                  transitionDest <- [ 0 .. min (nextUnusedNode eventRecord) (numNodes - 1) ]
                  pure $ Transition transitionWrite transitionDir transitionDest

            nextTransition <-
              maybe possibleTransitions pure $
                Map.lookup nextTransitionIndex $ partialTransitions incompleteTM

            let
              newIncompleteTM :: IncompleteTM
              newIncompleteTM =
                incompleteTM
                  { partialTransitions =
                    Map.insert nextTransitionIndex nextTransition $ partialTransitions incompleteTM
                  }

              newEventRecord :: EventRecord'
              newEventRecord =
                eventRecord <> EventRecord' (movement nextTransition /= None) (dest nextTransition + 1)

            runAndGenerate (followTransition runtime nextTransition) newIncompleteTM newEventRecord (remainingSteps - 1)


generate ::
  (
  )
    => StateIndex -> Integer -> [ TM ]
generate n m =
  do
    terminalStateIndex <- [ 0 .. pred n ]
    terminalTrigger <- [ True, False ]
    let
      terminalTransition =
        TransitionIndex
          terminalStateIndex
          terminalTrigger

    guard $ terminalTransition /= TransitionIndex start False

    limitIndex <- [ 0 .. pred n ]
    let
      go ::
        ( )
          => Bool -- Whether the machine is in limiting or finite behavior
          -> Bool -- Whether the halt state is accessible
          -> EventRecord
          -> Map TransitionIndex Transition
          -> Queue (Bool, TransitionIndex)
          -> [ Map TransitionIndex Transition ]
      go limiting haltHit eventRecord partialMachine liveTransitions =
        case
          pop liveTransitions
        of
          Nothing ->
            if
              limiting
              && writtenStatus eventRecord == pretraversalWrittenStatus eventRecord
            then
              [ partialMachine
              | Map.size partialMachine == n * 2 - 1 -- Thm. 1
              , haltHit
              , tWritten $ writtenStatus eventRecord -- Machines must write a 1 to the tape. Thm. ??
              , directionUsed eventRecord -- Machines must move at some point. Thm. ??
              ]
            else
              go True haltHit ( eventRecord { pretraversalWrittenStatus = writtenStatus eventRecord } ) partialMachine $
                map ((,) $ tWritten $ writtenStatus eventRecord) $
                  ( filter (flip Map.notMember partialMachine) $
                    map (TransitionIndex limitIndex) $
                      [ True | tWritten $ writtenStatus eventRecord ] ++
                      [ False | fWritten $ writtenStatus eventRecord ]
                  )
          Just ((strong, topTransition), remainingTransitions) ->
            if
              topTransition == terminalTransition
              && not strong
            then
              []
            else if
              topTransition == terminalTransition
              || Set.member topTransition (stronglyAccessibleTransitions eventRecord)
              || ( not strong && Map.member topTransition partialMachine )
            then
              go limiting (topTransition == terminalTransition || haltHit) eventRecord partialMachine remainingTransitions
            else
              case
                Map.lookup topTransition partialMachine
              of
                Just (Transition write move dest) ->
                  let
                    newEventRecord =
                      eventRecord
                      { stronglyAccessibleTransitions =
                        Set.insert topTransition $
                          stronglyAccessibleTransitions eventRecord
                      }
                    newTransitions =
                      map ((,) strong) $
                        filter (flip Set.notMember $ stronglyAccessibleTransitions eventRecord) $ -- Must be have written a t
                          map (TransitionIndex dest) $
                            case
                              move
                            of
                              None ->
                                [ write
                                ]
                              _ ->
                                [ True
                                , False
                                ]
                  in
                  assert strong $
                    go
                      limiting
                      haltHit
                      newEventRecord
                      partialMachine
                      ( insert newTransitions remainingTransitions )
                Nothing ->
                  ( do
                    write <- [ True, False ]
                    move <-
                      if
                        directionUsed eventRecord
                      then
                        [ Up, Down, None ] 
                      else
                        [ Up, None ]
                    dest <- [ start .. min (minUnusedIndex eventRecord) (pred n) ]

                    guard $ not $
                      dest == stateTrigger topTransition
                      && write == valueTrigger topTransition
                      &&
                        case
                          Map.lookup ( TransitionIndex dest (not write) ) partialMachine
                        of
                          Nothing ->
                            False
                          Just (Transition w m t) ->
                            w /= write && ( m /= None || move /= None )

                    guard $ not $
                      write /= valueTrigger topTransition
                      && Map.lookup ( TransitionIndex dest write ) partialMachine == Just ( Transition (not write) None $ stateTrigger topTransition )

                    guard $ not $
                      write /= valueTrigger topTransition
                      && any
                        ( \ (Transition w m d) -> 
                          d == dest
                          && m == move
                          && w == write
                        )
                        partialMachine
                    guard $ not $ getAny $
                      Map.foldMapWithKey
                        ( \ (TransitionIndex _ trigger) (Transition w m d) -> 
                          Any $
                            d == dest
                            && m == move
                            && trigger /= w
                            && w == write
                        )
                        partialMachine

                    let
                      newEventRecord =
                        eventRecord
                          { minUnusedIndex =
                            max
                              ( minUnusedIndex eventRecord )
                              ( succ dest )
                          , directionUsed =
                            directionUsed eventRecord
                            || move /= None
                          , writtenStatus =
                            writtenStatus eventRecord
                              <> WrittenStatus (write == True) (write == False)
                          , stronglyAccessibleTransitions =
                            if
                              strong
                              || write == True
                            then
                              Set.insert topTransition $
                                stronglyAccessibleTransitions eventRecord
                            else
                              stronglyAccessibleTransitions eventRecord
                          }
                      newTransition =
                        Transition
                        { write =
                          write
                        , movement =
                          move
                        , dest =
                          dest
                        }
                      newPartialMachine =
                        Map.insert
                          topTransition
                          newTransition
                          partialMachine
                      newTransitions =
                        map ((,) $ strong || write == True) $
                          filter
                            ( if
                                strong || write == True
                              then
                                flip Set.notMember $ stronglyAccessibleTransitions eventRecord
                              else
                                flip Map.notMember newPartialMachine
                            ) $
                            map (TransitionIndex dest) $
                              case
                                move
                              of
                                None ->
                                  [ write
                                  ]
                                -- When moving we may reveal t / f, unless no transition yet writes a t.
                                _ ->
                                  if
                                    strong
                                  then
                                    [ True
                                    , False
                                    ]
                                  else
                                    [ False
                                    ]
                     
                    go
                      limiting
                      haltHit
                      newEventRecord
                      newPartialMachine
                      ( insert
                        newTransitions
                        remainingTransitions
                      )
                  )

    almostAllTransitions <- go False False (EventRecord False mempty mempty (succ start) Set.empty) Map.empty [ (False, TransitionIndex start False) ]

    let
      machine =
        TM
          { limit =
            limitIndex
          , transitions =
            almostAllTransitions
          , size =
            n
          }

    guard $
      case
        runTM m machine
      of
        HaltsIn x ->
          False
        RunsFor x ->
          True

    pure machine



main :: IO ()
main = do
  putStrLn "hello world"
