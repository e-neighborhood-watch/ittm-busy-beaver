module TM.Tape.IList where


import TM.Tape


data IListTape a
  = IListTape
    { headAndRight ::
      IList a
    , left ::
      IList a
    , offset ::
      Integer
    }


data IList a
  = a :~ IList a


instance Functor IList where
  fmap f (x :~ xs) =
    f x :~ fmap f xs


instance Functor IListTape where
  fmap f (IListTape hrs ls o) =
    IListTape (fmap f hrs) (fmap f ls) o


instance Tape IListTape where
  headLeft (IListTape hrs (l :~ ls) o) =
    IListTape (l :~ hrs) ls (o - 1)

  headRight (IListTape (h :~ rs) ls o) =
    IListTape rs (h :~ ls) (o + 1)

  readHead (IListTape (h :~ _) _ o) =
    h

  writeHead h (IListTape (_ :~ rs) ls o) =
    IListTape (h :~ rs) ls o

  newTape initial =
    let
      m =
        initial :~ m
    in
      IListTape m m 0

  headOffset =
    offset

