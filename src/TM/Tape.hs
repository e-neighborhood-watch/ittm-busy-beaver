module TM.Tape where


class Functor f => Tape f where
  headLeft :: Eq a => f a -> f a

  headRight :: Eq a => f a -> f a

  readHead :: f a -> a

  writeHead :: a -> f a -> f a

  newTape :: a -> f a

  headOffset :: f a -> Integer

  resetHead :: Eq a => f a -> f a
  resetHead tape
    | headOffset tape == 0 =
      tape
    | headOffset tape > 0 =
      resetHead $ headLeft tape
    | headOffset tape < 0 =
      resetHead $ headRight tape



