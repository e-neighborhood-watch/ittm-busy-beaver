module TM.Run where


import Control.Monad
import Data.Maybe
import qualified Data.Map as Map


import TM
import TM.Tape
import Ordinal


data ListTape a
  = ListTape
    { tapeHead ::
      a
    , right ::
      [ a ]
    , rightBound ::
      a
    , left ::
      [ a ]
    , leftBound ::
      a
    , offset ::
      Integer
    }


instance Functor ListTape where
  fmap f tape =
    ListTape
      { tapeHead =
        f $ tapeHead tape
      , right =
        f <$> right tape
      , rightBound =
        f $ rightBound tape
      , left =
        f <$> left tape
      , leftBound =
        f $ leftBound tape
      , offset =
        offset tape
      }


instance Tape ListTape where

  headLeft tape =
    cleanup $
      tape
        { tapeHead =
          fromMaybe (leftBound tape) $ listToMaybe $ left tape
        , left =
          drop 1 $ left tape
        , right =
          tapeHead tape : right tape
        , offset =
          offset tape - 1
        }
    where
      cleanup :: Eq a => ListTape a -> ListTape a
      cleanup tape
        | [ r ] <- right tape
        , r == rightBound tape
        =
          tape
            { right =
              []
            }
      cleanup tape =
        tape


  headRight tape =
    cleanup $
      tape
        { tapeHead =
          fromMaybe (rightBound tape) $ listToMaybe $ right tape
        , right =
          drop 1 $ right tape
        , left =
          tapeHead tape : left tape
        , offset =
          offset tape + 1
        }
    where
      cleanup :: Eq a => ListTape a -> ListTape a
      cleanup tape
        | [ l ] <- left tape
        , l == leftBound tape
        =
          tape
            { left =
              []
            }
      cleanup tape =
        tape


  readHead =
    tapeHead


  writeHead newHead tape =
    tape
      { tapeHead =
        newHead
      }


  newTape initial =
    ListTape
      { tapeHead =
        initial
      , right =
        []
      , rightBound =
        initial
      , left =
        []
      , leftBound =
        initial
      , offset =
        0
      }

  headOffset =
    offset


moveHead ::
  ( Tape f
  , Eq a
  )
    => Movement -> f a -> f a
moveHead Up =
  headRight
moveHead Down =
  headLeft
moveHead None =
  id


data Runtime tape a
  = Runtime
    { tape ::
      tape a
    , stateIndex ::
      StateIndex
    , time ::
      Ordinal
    }


initialRuntime :: Tape f => Runtime f Bool
initialRuntime =
  Runtime
    { tape =
      newTape False
    , stateIndex =
      start
    , time =
      Ordinal 0 0
    }


followTransition ::
  ( Tape f
  )
    => Runtime f Bool -> Transition -> Runtime f Bool
followTransition (Runtime prevTape _ (Ordinal w r)) (Transition write move dest) =
  Runtime
    { tape =
      moveHead move $
        writeHead write $
          prevTape
    , stateIndex =
      dest
    , time =
      Ordinal w (r + 1)
    }


runStep ::
  ( Tape f
  )
    => TM -> Runtime f Bool -> Maybe (Runtime f Bool)
runStep tm startRuntime =
  let
    nextTransition :: TransitionIndex
    nextTransition =
      TransitionIndex
        { stateTrigger =
          stateIndex startRuntime
        , valueTrigger =
          readHead $ tape $ startRuntime
        }
  in
    fmap (followTransition startRuntime) $ Map.lookup nextTransition $ transitions tm


tryToLimit :: TM -> Runtime ListTape Bool -> Maybe (Runtime ListTape Bool)
tryToLimit tm startRuntime =
  let
    nextTransitionIndex :: TransitionIndex
    nextTransitionIndex =
      TransitionIndex
        { stateTrigger =
          stateIndex startRuntime
        , valueTrigger =
          readHead $ tape $ startRuntime
        }
  in
    do
      Transition write move dest <- Map.lookup nextTransitionIndex (transitions tm)
      -- Self loops
      guard $ dest == stateTrigger nextTransitionIndex 
      case move of
        None ->
          do
            guard $
              write == valueTrigger nextTransitionIndex
            pure $
              Runtime
                { tape =
                  resetHead $ tape startRuntime
                , stateIndex =
                  limit tm
                , time =
                  let
                    Ordinal w r =
                      time startRuntime
                  in
                    Ordinal (w + 1) 0
                }
        Up ->
          do
            guard $
              [] == right (tape startRuntime)
              && valueTrigger nextTransitionIndex == rightBound (tape startRuntime)
            pure $
              Runtime
                { tape =
                  resetHead $
                    writeHead write $
                      (tape startRuntime)
                        { rightBound =
                          write
                        }
                , stateIndex =
                  limit tm
                , time =
                  let
                    Ordinal w r =
                      time startRuntime
                  in
                    Ordinal (w + 1) 0
                }
        Down ->
          do
            guard $
              [] == left (tape startRuntime)
              && valueTrigger nextTransitionIndex == leftBound (tape startRuntime)
            pure $
              Runtime
                { tape =
                  resetHead $
                    writeHead write $
                      (tape startRuntime)
                        { leftBound =
                          write
                        }
                , stateIndex =
                  limit tm
                , time =
                  let
                    Ordinal w r =
                      time startRuntime
                  in
                    Ordinal (w + 1) 0
                }
                


data Status
  = HaltsIn Ordinal
  | RunsFor Ordinal
  deriving
    ( Show
    )


runTM :: Integer -> TM -> Status
runTM =
  go initialRuntime
  where
    go :: Runtime ListTape Bool -> Integer -> TM -> Status
    go prevRuntime maxSteps tm
      | maxSteps < 0 =
        RunsFor $
          time prevRuntime
      | otherwise =
        case
          tryToLimit tm prevRuntime
        of
          Just newRuntime ->
            go newRuntime (maxSteps - 1) tm
          Nothing ->
            case
              runStep tm prevRuntime
            of
              Nothing ->
                HaltsIn $ time prevRuntime

              Just newRuntime ->
                go newRuntime (maxSteps - 1) tm
