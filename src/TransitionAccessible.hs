module TransitionAccessible where


import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Set
  ( Set
  )


import TM


type MachineAccessibility =
  Set TransitionIndex


allTransitionsAccessible ::
  ( )
    => TM -> Bool
allTransitionsAccessible tm =
  let
    omegaAccessSet ::
      ( )
        => Set TransitionIndex
    omegaAccessSet =
      Set.insert (TransitionIndex (limit tm) True) $
        Set.insert (TransitionIndex (limit tm) False) $
          goWithInsert Set.empty $ TransitionIndex start False
  in
    Set.size
      ( Set.union
        (go omegaAccessSet $ TransitionIndex (limit tm) True)
        (go omegaAccessSet $ TransitionIndex (limit tm) False)
      )
      == 2 * size tm
  where
    goWithInsert ::
      ( )
        => MachineAccessibility -> TransitionIndex -> MachineAccessibility
    goWithInsert ma i =
      go (Set.insert i ma) i


    go ::
      ( )
        => MachineAccessibility -> TransitionIndex -> MachineAccessibility
    go ma i =
      case
        Map.lookup i (transitions tm)
      of
        Nothing ->
          ma
        Just (Transition write None dest) ->
          let
            nextTransition =
              TransitionIndex dest write
          in
            if
              Set.member nextTransition ma
            then
              ma
            else
              goWithInsert ma nextTransition
        Just (Transition write dir dest) ->
          case
            ( Set.member (TransitionIndex dest True) ma
            , Set.member (TransitionIndex dest False) ma
            )
          of
            (False, False) ->
              Set.union
                (goWithInsert ma $ TransitionIndex dest False)
                (goWithInsert ma $ TransitionIndex dest True)
            (True, False) ->
              (goWithInsert ma $ TransitionIndex dest False)
            (False, True) ->
              (goWithInsert ma $ TransitionIndex dest True)
            (True, True) ->
              ma
      
