module Deque
  ( FixedSizeDeque (..)
  , FastDeque
  )
  where


import qualified Data.List.NonEmpty as NonEmpty
import Data.List.NonEmpty
  ( NonEmpty (..)
  , (<|)
  )


class FixedSizeDeque f where
  fromNonEmpty :: NonEmpty a -> f a
  rollForward :: f a -> f a
  rollBackward :: f a -> f a
  toNonEmpty :: f a -> NonEmpty a
  head :: f a -> a
  last :: f a -> a


data FastDeque a =
  FastDeque
    { front ::
      [ a ]
    , back ::
      NonEmpty a
    , original ::
      NonEmpty a
    , originalRev ::
      NonEmpty a
    }


instance FixedSizeDeque FastDeque where
  fromNonEmpty xs =
    FastDeque
      { front =
        []
      , back =
        NonEmpty.reverse xs
      , original =
        xs
      , originalRev =
        NonEmpty.reverse xs
      }

  rollForward ( FastDeque [] _ ( x :| xs ) originalRev ) =
    FastDeque xs ( x :| [] ) ( x :| xs ) originalRev
  rollForward ( FastDeque ( x : xs ) back original originalRev ) =
    FastDeque xs ( x <| back ) original originalRev

  rollBackward ( FastDeque _ ( _ :| [] ) original originalRev ) =
    FastDeque [] originalRev original originalRev
  rollBackward ( FastDeque front ( x :| x' : xs ) original originalRev ) =
    FastDeque ( x : front ) ( x' :| xs ) original originalRev

  toNonEmpty ( FastDeque front back _ _ ) =
    foldr (<|) (NonEmpty.reverse back) front

  head ( FastDeque [] _ ( x :| _ ) _ ) =
    x
  head ( FastDeque ( x : _ ) _ _ _ ) =
    x

  last ( FastDeque _ ( x :| _ ) _ _ ) =
    x
