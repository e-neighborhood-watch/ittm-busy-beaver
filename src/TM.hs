module TM where


import Data.Map
  ( Map
  )
import Data.Foldable


type StateIndex =
  Int


data TransitionIndex
  = TransitionIndex
    { stateTrigger ::
      StateIndex
    , valueTrigger ::
      Bool
    }
  deriving
    ( Show
    , Eq
    , Ord
    )


data Movement
  = Up
  | Down
  | None
  deriving
    ( Show
    , Eq
    , Ord
    )


data Transition
  = Transition
    { write ::
      Bool
    , movement ::
      Movement
    , dest ::
      StateIndex
    }
  deriving
    ( Show
    , Eq
    , Ord
    )


data TM
  = TM
    { limit ::
      StateIndex
    , transitions ::
      Map TransitionIndex Transition
    , size ::
      StateIndex
    }
  deriving
    ( Show
    , Eq
    , Ord
    )


showFullTransition :: TransitionIndex -> Transition -> String
showFullTransition (TransitionIndex fromState fromValue) (Transition toValue move toState) =
  fold
    [ "[ q"
    , show fromState
    , " -> q"
    , show toState
    , " ]: "
    , prettyValue fromValue
    , " -> "
    , prettyValue toValue
    , prettyMove
    ]
  where
    prettyValue :: Bool -> String
    prettyValue val =
      if
        val
      then
        "1"
      else
        "0"
    prettyMove :: String
    prettyMove =
      case
        move
      of
        Up ->
          ", R"
        Down ->
          ", L"
        None ->
          ""


start ::
  (
  )
    => StateIndex
start =
  0
