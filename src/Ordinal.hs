module Ordinal where

data Ordinal
  = Ordinal
    { omega ::
      Integer
    , rational ::
      Integer
    }
  deriving
    ( Eq
    , Ord
    )

instance Show Ordinal where
  show (Ordinal w r)
    | w > 1
    =
      "w" ++ show w ++ "+" ++ show r
    | otherwise
    =
      show r
