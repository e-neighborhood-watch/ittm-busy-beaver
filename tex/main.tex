\documentclass{article}

\usepackage{amsmath} 
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{tikz}

\usetikzlibrary{automata, positioning}

\newtheorem{theorem}{Theorem}
\newtheorem{corollary}{Corollary}[theorem]
\newtheorem{definition}{Definition}
\newtheorem{machine}{Machine}

\begin{document}

\section{Handmade Machines}

Before doing any of the theoretical ground work we will establish a few champion machines found by hand.
These machines will help us in establishing lower bounds which will be useful for computation.

\begin{machine}[1-State Champion]
  \label{1champ}
  \hfill
  \begin{center}
    \begin{tikzpicture}
      \node (q0)
          [ state
          , initial
          , initial text=start/limit
          ]
          {$q_0$};
      \path
        (q0) edge [loop above] node {$0\rightarrow 1, R$} ();
    \end{tikzpicture}
  \end{center}
\end{machine}
\begin{proof}
  The condition of the machine for $n < \omega$ can be determined inductively.
  Assume that at time $n$ we have written $1$ to the first $n$ cells and no other cells and the read head is at cell $n$.
  The read head is on a $0$.
  The machine then writes a $1$ to cell $n$ and moves to cell $n+1$.
  Thus at time $n+1$ we have written $1$ to the first $n+1$ cells and no other cells and the read head is at cell $n+1$.
  The value of every cell $k$ at time $\omega$ where $k \geq 0$ is $1$.
  Thus at time $\omega$ the cell $0$ is on, and the machine halts.
\end{proof}

This machine can even be shown to be optimal by hand.
While a couple of tricks can reduce the search space, there are only 24 machines and it is easy enough to simply check all of them.
We will not include such a check since this result will be confirmed by later results.

\begin{machine}[2-State Champion]
  \label{2champ}
  \hfill
  \begin{center}
    \begin{tikzpicture}[node distance=2cm]
      \node (q0)
            [ state
            , initial
            , initial text=start/limit
            ]
            {$q_0$};
      \node (q1)
            [ state
            , right of=q0
            ]
            {$q_1$};
      \path[->]
      (q0) edge [loop above] node [above] {$1\rightarrow 0, R$} ()
      (q0) edge [bend right] node [below] {$0\rightarrow 1, L$} (q1)
      (q1) edge [bend right] node [above] {$0\rightarrow 1$} (q0);
    \end{tikzpicture}
  \end{center}
\end{machine}

\begin{machine}[3-State Champion]
  \label{3champ}
  \hfill
  \begin{center}
    \begin{tikzpicture}[node distance=2cm]
      \node (q0)
            [ state
            , initial
            , initial text=start/limit
            ]
            {$q_0$};
      \node (q1)
            [ state
            , right of=q0
            ]
            {$q_1$};
      \node (q2)
            [ state
            , right of=q1
            ]
            {$q_2$};
      \path[->]
        (q0) edge [bend left] node [above] {$0\rightarrow 0, R$} (q1)
        (q1) edge [loop above] node [above] {$1\rightarrow 0, L$} ()
        (q1) edge [bend left] node [above] {$0\rightarrow 1$} (q2)
        (q2) edge [loop above] node [above] {$0\rightarrow 0$} ()
        (q2) edge [loop below] node [below] {$1\rightarrow 0$} ();
    \end{tikzpicture}
  \end{center}
\end{machine}

\begin{machine}[4-State Champion]
  \label{4champ}
  \hfill
  \begin{center}
    \begin{tikzpicture}[node distance=2cm]
      \node (q0)
            [ state
            , initial
            , initial text=start/limit
            ]
            {$q_0$};
      \node (q1)
            [ state
            , right of=q0
            ]
            {$q_1$};
      \node (q2)
            [ state
            , right of=q1
            ]
            {$q_2$};
      \node (q3)
            [ state
            , right of=q2
            ]
            {$q_3$};
      \path[->]
      (q0) edge [bend left] node [above] {$0\rightarrow 1, R$} (q2)
      (q0) edge [bend right] node [below] {$1\rightarrow 1, R$} (q1)
      (q1) edge [bend right] node [below] {$1\rightarrow 0, R$} (q2)
      (q2) edge [loop above] node [above] {$1\rightarrow 0, R$} ()
      (q2) edge [bend right] node [below] {$0\rightarrow 1$} (q3)
      (q3) edge [loop above] node [above] {$1\rightarrow 0$} ()
      (q3) edge [loop below] node [below] {$0\rightarrow 0, L$} ();
    \end{tikzpicture}
  \end{center}
\end{machine}

\section{Elementary Results}

\begin{definition}
  A \textbf{missing transition} of some machine $T$ is a pair $(A, \sigma) : S_T \times \Sigma_T$ such that $\delta_T(A, \sigma)$ is undefined.
\end{definition}

\setcounter{theorem}{-1}

\begin{theorem}
  An $n$-state machine with no missing transitions, will never halt.
\end{theorem}
\begin{proof}
  If a machine $T$ has no missing transition, $\delta_T$ is defined for all inputs
  Thus at any time $t$ there result of applying $\delta_T$ to the current state and symbol will yield a new state and instructions.
  Thus at time $t$ the machine cannot halt.
  Thus the machine cannot halt at any time.
\end{proof}

\begin{theorem}
  \label{transitions}
  If an $n$-state machine has more than $1$ missing transitions and halts, there exists an $n$-state machine with fewer missing transitions which halts and runs for longer.
\end{theorem}
\begin{proof}
  Let $T$ be an $n$-state machine with more than $1$ transition.
  Let $(A, \sigma)$ be the missing transition which causes the machine to halt at time $t_h$.
  Since ITTMs are deterministic this state and symbol pair is unique.
  Let $(B, \gamma)$ be a missing transition distinct from $(A, \sigma)$.
  Since there are at least $2$ missing transitions such a transition exists but need not be unique.
  Let us consider the machine $T'$ which is identical to $T$ except with an additional transition from $A$ to $B$, triggered by $\sigma$ and writing $\gamma$ with no movement.
  $T'$ will behave identically to $T$ for time before $t_h$.
  At time $t_h$ the new transition will be triggered instead of halting and it will transition to state $B$ writing $\gamma$ to the tape.
  Since $T'$ lacks any transition corresponding to the pair $(B, \gamma)$, the machine will halt then at time $t_h+1$.
  Thus $T'$ is a $n$-state machine with more transitions which halts and runs for longer.
\end{proof}

\begin{theorem}
  If an $n$-state machine halts without triggering every transition, there exists an $n$-state machine which halts and runs for longer.
\end{theorem}
\begin{proof}
  If $T$ is a machine that halts without triggering the transition $(A, \sigma)$, then a machine $T'$ which is identical to $T$ except the transition at $(A, \sigma)$ has been removed behaves identically.
  Since $T'$ has fewer transitions, and thus more than $1$ missing transition there exists a $n$-state machine which runs longer than $T'$ and thus longer than $T$.
\end{proof}

\begin{theorem}
  \label{linearmonotonic}
  $S_\infty(n+1) \geq S_\infty(n)+2$
\end{theorem}
\begin{proof}
  There must be some $n$ state machine, $T$, which takes $S_\infty(n)$ steps to halt.
  We can agument $T$ by adding a new state and leaving all existing transitions the same.
  This new machine $T'$ is an $n+1$ state machine, which --- having the same transitions as $T$ --- takes $S_\infty(n)$ steps to halt.
  This new machine has 3 missing transitions (the transition $T$ was missing plus the two transitions from the new state).
  Thus by \textbf{Theorem \ref{transitions}} there exists an $n+1$ state machine which halts in $S_\infty(n)+2$ steps.
  Thus $S_\infty(n+1)$ is at least $S_\infty(n)+2$.
\end{proof}

This can be combined with our champions to find create some practical limits.

\begin{corollary}
  \[\forall n . S_\infty(n) \geq \omega\]

  No busy beaver machine can halt in finite time.
\end{corollary}
\begin{proof}
  This can easily be proven by induction.
  By Machine \ref{1champ}, $S_\infty(1)$ is infinite.
  Theorem \ref{linearmonotonic} provides an inductive step.
  Thus $\forall n . S_\infty(n) \geq \omega$
\end{proof}

This means when simulating machines we can discard any machine that halts in finite time.

\begin{corollary}
  \[\forall n > 1 . S_\infty(n) \geq \omega^2 \]
\end{corollary}
\begin{proof}
  This can be proven in the same way as the last.
  By Machine \ref{2champ}, $S_\infty(2) > \omega^2$.
  Theorem \ref{linearmonotonic} provides an inductive step.
  Thus $\forall n > 1 . S_\infty(n) \geq \omega^2$
\end{proof}

Since our algorithm is incapable of simulating machines to $\omega^2$, this means except in the special case of $S_\infty(1)$ we can discard any machine at all that halts in our simulation.


\end{document}
