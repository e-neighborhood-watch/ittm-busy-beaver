{-# OPTIONS --safe --exact-split --without-K #-}


module Core where


data ⊥ : Set where


¬ : Set → Set
¬ A = A → ⊥


absurd : { A : Set } → ⊥ → A
absurd ()


data ℕ : Set where
  zero : ℕ
  succ : ℕ → ℕ
{-# BUILTIN NATURAL ℕ #-}


data Bool : Set where
  true false : Bool
{-# BUILTIN BOOL Bool #-}
{-# BUILTIN TRUE true #-}
{-# BUILTIN FALSE false #-}


data ℤ : Set where
  pos : ℕ → ℤ
  negsucc : ℕ → ℤ
{-# BUILTIN INTEGER ℤ #-}
{-# BUILTIN INTEGERPOS pos #-}
{-# BUILTIN INTEGERNEGSUC negsucc #-}


data Σ {A : Set} (B : A → Set) : Set where
  _,_ : (a : A) → B a → Σ B
infixr 20 _,_


data _+_ (A B : Set) : Set where
  inl : A → A + B
  inr : B → A + B


data _≡_ { A : Set } : A → A → Set where
  refl : ( a : A ) → a ≡ a
{-# BUILTIN EQUALITY _≡_ #-}


data Maybe ( A : Set ) : Set where
  nothing : Maybe A
  just : A → Maybe A


if_then_else_ : { a : Set } → Bool → a → a → a
if true then x else _ =
  x
if false then _ else y =
  y

_×_ : Set → Set → Set
A × B = Σ ( λ ( _ : A ) → B )
infixr 20 _×_


add : ℕ → ℕ → ℕ
add 0 m = m
add (succ n) m = succ (add n m)
{-# BUILTIN NATPLUS add #-}


_==_ : ℕ → ℕ → Bool
zero == zero =
  true
succ m == succ n =
  m == n
zero == succ _ =
  false
succ _ == zero =
  false
{-# BUILTIN NATEQUALS _==_ #-}


_𝕫=_ : ℤ → ℤ → Bool
pos x 𝕫= pos y =
  x == y
negsucc x 𝕫= negsucc y =
  x == y
pos _ 𝕫= negsucc _ =
  false
negsucc _ 𝕫= pos _ =
  false


data _<ₙ_ : ℕ → ℕ → Set where
  0<s : { n : ℕ } → 0 <ₙ succ n
  s<s : { n₁ n₂ : ℕ } → n₁ <ₙ n₂ → succ n₁ <ₙ succ n₂


pattern 1<s = s<s 0<s
pattern 2<s = s<s (s<s 0<s)
pattern 3<s = s<s (s<s (s<s 0<s))


LessThan : ℕ → Set
LessThan n =
  Σ ( λ ( m : ℕ ) → m <ₙ n )


record Number ( A : Set ) : Set₁ where
  field
    Constraint : ℕ → Set
    fromNat : ( n : ℕ ) → ⦃ _ : Constraint n ⦄ → A

open Number {{...}} public using (fromNat)
{-# BUILTIN FROMNAT fromNat #-}


data ⊤ : Set where
  tt : ⊤


instance
  tt⊤ : ⊤
  tt⊤ = tt


instance
  Numberℕ : Number ℕ
  Numberℕ = record
    { Constraint = λ _ → ⊤
    ; fromNat = λ x → x
    }


instance
  0<s<ₙ : { n : ℕ } → 0 <ₙ succ n
  0<s<ₙ = 0<s

  s<s<ₙ : { n m : ℕ } → ⦃ n <ₙ m ⦄ → succ n <ₙ succ m
  s<s<ₙ ⦃ n<m ⦄ = s<s n<m


instance
  NumberLessThan : { n : ℕ } → Number (LessThan n)
  NumberLessThan {n} .Number.Constraint m = m <ₙ n
  NumberLessThan {_} .Number.fromNat m {{m<n}} = ( m , m<n )
