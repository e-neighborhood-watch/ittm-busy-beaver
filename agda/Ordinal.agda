{-# OPTIONS --safe --exact-split --without-K #-}


module Ordinal where


open import Core


data Ordinal : Set
exp : Ordinal → Ordinal


data _<_ : Ordinal → Ordinal → Set


data Ordinal where
  zero : Ordinal
  cantor : (exponent : Ordinal) → ℕ → (rest : Ordinal) → exp rest < exponent → Ordinal


data _<_ where
  0<c : { e x : Ordinal } { n : ℕ } { con : exp x < e } → zero < cantor e n x con
  c<₁c : { e₁ e₂ x₁ x₂ : Ordinal } { n₁ n₂ : ℕ } { con₁ : exp x₁ < e₁ } { con₂ : exp x₂ < e₂ } → e₁ < e₂ → cantor e₁ n₁ x₁ con₁ < cantor e₂ n₂ x₂ con₂
  c<₂c : { e₁ e₂ x₁ x₂ : Ordinal } { n₁ n₂ : ℕ } { con₁ : exp x₁ < e₁ } { con₂ : exp x₂ < e₂ } → e₁ ≡ e₂ → n₁ <ₙ n₂ → cantor e₁ n₁ x₁ con₁ < cantor e₂ n₂ x₂ con₂
  c<₃c : { e₁ e₂ x₁ x₂ : Ordinal } { n₁ n₂ : ℕ } { con₁ : exp x₁ < e₁ } { con₂ : exp x₂ < e₂ } → e₁ ≡ e₂ → n₁ ≡ n₂ → x₁ < x₂ → cantor e₁ n₁ x₁ con₁ < cantor e₂ n₂ x₂ con₂


exp zero = zero
exp (cantor ord _ _ _) = ord
