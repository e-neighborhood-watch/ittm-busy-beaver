{-# OPTIONS --safe --exact-split --without-K #-}


module ITTM where


open import Core
open import Ordinal


data Movement : Set where
  L R N : Movement


data Alphabet ( n : ℕ ) : Set where
  symbol : LessThan n → Alphabet n


data State ( n : ℕ ) : Set where
  q : LessThan n → State n


q₀ : { n : ℕ } → State (succ n)
q₀ {n} = q 0


q₁ : { n : ℕ } → State (succ (succ n))
q₁ {n} = q 1


q₂ : { n : ℕ } → State (succ (succ (succ n)))
q₂ {n} = q 2


q₃ : { n : ℕ } → State (succ (succ (succ (succ n))))
q₃ {n} = q 3


0ₜ : { n : ℕ } → Alphabet (succ n)
0ₜ { n } = symbol 0


1ₜ : { n : ℕ } → Alphabet (succ (succ n))
1ₜ {n} = symbol 1


ITTM : ℕ → ℕ → Set
ITTM as n =
  State n × State n × (State n → Alphabet as → Maybe (State n × Alphabet as × Movement))


machineCondition : ℕ → ℕ → Set
machineCondition as n =
  State n × ℤ × (ℤ → Alphabet as)


state : { as n : ℕ } → machineCondition as n → State n
state ( s , _ ) = s


tapeHead : { as n : ℕ } → machineCondition as n → ℤ
tapeHead ( _ , th , _ ) = th


tape : { as n : ℕ } → machineCondition as n → (ℤ → Alphabet as)
tape ( _ , _ , t ) = t


_≅ₜ_ : { as : ℕ } → (ℤ → Alphabet as) → (ℤ → Alphabet as) → Set
t₁ ≅ₜ t₂ = ( z : ℤ ) → t₁ z ≡ t₂ z


reflₜ : { as : ℕ } → ( t : ℤ → Alphabet as ) → t ≅ₜ t
reflₜ t z = refl (t z)


data _≅ₘ_ { as n : ℕ } : Maybe (machineCondition as n) → Maybe (machineCondition as n) → Set where
  n≅ₘn : nothing ≅ₘ nothing
  j≅ₘj : ( c₁ c₂ : machineCondition as n ) → state c₁ ≡ state c₂ → tapeHead c₁ ≡ tapeHead c₂ → tape c₁ ≅ₜ tape c₂ → just c₁ ≅ₘ just c₂


reflₘ : { as n : ℕ } ( c : Maybe (machineCondition as n) ) → c ≅ₘ c
reflₘ nothing = n≅ₘn
reflₘ (just c@( s , th , t )) = j≅ₘj c c (refl s) (refl th) (reflₜ t)


module TakeStep where
  go : { as n : ℕ } → Maybe (State n × Alphabet as × Movement) → ℤ → (ℤ → Alphabet as) → Maybe (machineCondition as n)
  go nothing _ _ =
    nothing
  go (just (next , write , move)) head tape =
    just
      ( next
      , newHead move head
      , λ (y : ℤ) →
        if
          y 𝕫= head
        then
          write
        else
          tape y
      )
    where
      newHead : Movement → ℤ → ℤ
      newHead N n =
        n
      newHead L (pos (succ n)) =
        pos n
      newHead L (pos zero) =
        negsucc zero
      newHead L (negsucc n) =
        negsucc (succ n)
      newHead R (pos n) =
        pos (succ n)
      newHead R (negsucc (succ n)) =
        negsucc n
      newHead R (negsucc zero) =
        pos zero

  takeStep : { as : ℕ } → { n : ℕ } → ITTM as n → machineCondition as n → Maybe (machineCondition as n)
  takeStep { as } { n } (_ , _ , δ) (current , head , tape) =
    go (δ current (tape head)) head tape

open TakeStep using (takeStep)

DoesNotHalt₁ : { as n : ℕ } → ITTM as n → Set
DoesNotHalt₁ {as} {n} machine =
  (condition : machineCondition as n) → Σ ( λ ( nextCondition : machineCondition as n ) → takeStep machine condition ≅ₘ just nextCondition )


MissingPair : { as n : ℕ } → ITTM as n → Set
MissingPair {as} {n} ( _ , _ , δ ) =
  Σ { State n × Alphabet as } ( λ { ( state , read ) → δ state read ≡ nothing } )


Theorem0 : { as n : ℕ } → ( machine : ITTM as n ) → ¬ (MissingPair machine) → DoesNotHalt₁ machine
Theorem0 {as} {n} machine@( _ , _ , δ ) noMissingPairs condition@( current , head , tape ) =
  go₁ (δ current (tape head)) (refl (δ current (tape head))) (takeStep machine condition) (reflₘ (takeStep machine condition))
  where
    go₁ : (transition : Maybe (State n × Alphabet as × Movement)) → δ current (tape head) ≡ transition → (nextState : Maybe (machineCondition as n)) → TakeStep.go transition head tape ≅ₘ nextState → Σ ( λ nextState → TakeStep.go transition head tape ≅ₘ just nextState )
    go₁ nothing transitionEq _ _ = absurd (noMissingPairs ( ( current , tape head ) , transitionEq ))
    go₁ (just ( _ , _ , _ )) _ (just nextState) conditionEq = ( nextState , conditionEq )


champion₁ : ITTM 2 1
champion₁ = ( q₀ , q₀ , δ )
  where
    δ : State 1 → Alphabet 2 → Maybe (State 1 × Alphabet 2 × Movement)
    δ (q ( 0 , 0<s )) (symbol ( 0 , 0<s )) = just ( q₀ , 0ₜ , L )
    δ (q ( 0 , 0<s )) (symbol ( 1 , 1<s )) = nothing


champion₂ : ITTM 2 2
champion₂ = ( q₀ , q₀ , δ )
  where
    δ : State 2 → Alphabet 2 → Maybe (State 2 × Alphabet 2 × Movement)
    δ (q ( 0 , 0<s )) (symbol ( 0 , 0<s )) = just ( q₁ , 1ₜ , L )
    δ (q ( 0 , 0<s )) (symbol ( 1 , 1<s )) = just ( q₀ , 0ₜ , R )
    δ (q ( 1 , 1<s )) (symbol ( 0 , 0<s )) = just ( q₀ , 1ₜ , N )
    δ (q ( 1 , 1<s )) (symbol ( 1 , 1<s )) = nothing


champion₃ : ITTM 2 3
champion₃ = ( q₀ , q₀ , δ )
  where
    δ : State 3 → Alphabet 2 → Maybe (State 3 × Alphabet 2 × Movement)
    δ (q ( 0 , 0<s )) (symbol ( 0 , 0<s )) = just ( q₁ , 0ₜ , R )
    δ (q ( 0 , 0<s )) (symbol ( 1 , 1<s )) = nothing
    δ (q ( 1 , 1<s )) (symbol ( 0 , 0<s )) = just ( q₂ , 1ₜ , N )
    δ (q ( 1 , 1<s )) (symbol ( 1 , 1<s )) = just ( q₁ , 0ₜ , L )
    δ (q ( 2 , 2<s )) (symbol ( 0 , 0<s )) = just ( q₂ , 0ₜ , N )
    δ (q ( 2 , 2<s )) (symbol ( 1 , 1<s )) = just ( q₂ , 0ₜ , N )


champion₄ : ITTM 2 4
champion₄ = ( q₀ , q₀ , δ )
  where
    δ : State 4 → Alphabet 2 → Maybe (State 4 × Alphabet 2 × Movement)
    δ (q ( 0 , 0<s )) (symbol ( 0 , 0<s )) = just ( q₂ , 1ₜ , R )
    δ (q ( 0 , 0<s )) (symbol ( 1 , 1<s )) = just ( q₁ , 1ₜ , R )
    δ (q ( 1 , 1<s )) (symbol ( 0 , 0<s )) = nothing
    δ (q ( 1 , 1<s )) (symbol ( 1 , 1<s )) = just ( q₂ , 0ₜ , R )
    δ (q ( 2 , 2<s )) (symbol ( 0 , 0<s )) = just ( q₃ , 1ₜ , N )
    δ (q ( 2 , 2<s )) (symbol ( 1 , 1<s )) = just ( q₂ , 0ₜ , R )
    δ (q ( 3 , 3<s )) (symbol ( 0 , 0<s )) = just ( q₃ , 0ₜ , L )
    δ (q ( 3 , 3<s )) (symbol ( 1 , 1<s )) = just ( q₃ , 0ₜ , N )
